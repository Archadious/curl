<?php

// Encypt a string using GnuPG
//-----------------------------------------------------------------------------
class Encryption {


  // Instance variables
  var $handle = NULL;  // instance of the GnuPG object


  // Constructor
  function __construct( $fingerprint ) {
    //    $CONFIG[ 'gnupg_home' ] = '/home/daniel/.gnupg';
    $this->handle = new gnupg();  // create an instance of gnupg
    $this->handle->addencryptkey( $fingerprint );
  }  // end constructor


  // Encrypt the data using the provided GnuPG fingerprint
  function encrypt( $clear ) {
    return $this->handle->encrypt( $clear );
  }  // end encrypt


  // Decrypt using the provided GnuPG fingerprint
  function decrypt( $secret ) {
    return $this->handle->decrypt( $secret );
  }  // end decrypt


  // Return the instance as a string
  function __toString( ) {
    return $this->handle;
  }  // end __toString


}  // end Encryption


class BluejayPG extends Encryption {

  const Fingerprint = 'archadious@gmail.com';

  function __construct( ) {
    parent::__construct( BluejayPG::Fingerprint );
  }  // end __construct

}  // end BluejayPG


//$b = new BluejayPG( );

$bluejay_fingerprint = 'archadious@gmail.com';

$b = new Encryption( $bluejay_fingerprint );

$data = "Red Green Black Brown";
$secret = $b->encrypt( $data );
$clear = $b->decrypt( $secret );

printf( "\nEncrypted: %s\n", $secret );
printf( "\nDecrypted: %s\n", $clear );
